### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ abe3c292-cef2-11eb-1d53-ad16b647f780
begin
	using Images
	using ImageIO
	using Flux
	using Flux.Data: DataLoader
	using Flux: onehotbatch, onecold, logitcrossentropy
    using Statistics, Random
	using Flux: @epochs
	using EvalMetrics
	#using CUDAapi
end

# ╔═╡ af182110-cf8b-11eb-069a-3b6009262180
md"# Data Preprocessing"

# ╔═╡ fac58bf2-cef2-11eb-1e70-d38115051d1c
begin
	#Assumes current directory contains both image-containing folders.
	base_path = pwd()
	#Finds all file names in given path
	file_normal = readdir(base_path*"\\chest_xray\\test\\NORMAL")
	file_pneumonia = readdir(base_path*"\\chest_xray\\test\\PNEUMONIA")

	#Joins the paths with the file names
	path_x_test = base_path*"\\chest_xray\\test\\NORMAL\\".*file_normal
	path_y_test = base_path*"\\chest_xray\\test\\PNEUMONIA\\".*file_pneumonia

	x_normal_test = path_x_test
	y_pneumonia_test = path_y_test
	#Finds all file names in given path
	file_normal_train = readdir(base_path*"\\chest_xray\\train\\NORMAL")
	file_pneumonia_train = readdir(base_path*"\\chest_xray\\train\\PNEUMONIA")

	#Joins the paths with the file names
	path_x_normal_train = base_path*"\\chest_xray\\train\\NORMAL\\".*file_normal_train
	path_y_pneumonia_train = base_path*"\\chest_xray\\train\\PNEUMONIA\\".*file_pneumonia_train

	x_train = path_x_normal_train
	y_train  = path_y_pneumonia_train
	x_test_final = [x_normal_test; y_pneumonia_test]
	x_train_final = [path_x_normal_train; path_y_pneumonia_train]
end

# ╔═╡ a3a61330-d20d-11eb-1b4e-3102dcb98496
md"#  Creating Labels "

# ╔═╡ 2e113f0e-cf69-11eb-0c3f-7d36ff1444c6
pneuu_labels_train = ["pneumonia"
	for file_pneumonia_train in file_pneumonia_train]

# ╔═╡ 4c6cd2f0-cf6c-11eb-301d-97e7a2c385e5
normal_labels_train = ["normal" for file_normal_train in file_normal_train]

# ╔═╡ 5f453200-cf6c-11eb-09a8-77afa3bc3e87
pneuu_labels_test = ["pneumonia" for x_normal_test in x_normal_test]

# ╔═╡ 5a494160-cf6c-11eb-1392-bfb16b22b670
normal_labels_test = ["normal" for path_y_test in path_y_test]

# ╔═╡ 6ccb9c30-cef3-11eb-0fb7-1b3d978968e6
function process_image(path)
    img = load(path)
    img = Gray.(img)
    img = imresize(img,(28,28))
    img = Flux.unsqueeze(Float32.(img), 3)
	
    #img = convert(Array{Float64,1},img)
	
    return img
end

# ╔═╡ 57d815f0-d1bf-11eb-3b06-f90bec96e559
Ytrains =[normal_labels_train; pneuu_labels_train]

# ╔═╡ b4a3b350-d153-11eb-3714-1790a254a71f
Ytests =[normal_labels_test; pneuu_labels_test]

# ╔═╡ ceda4440-cf56-11eb-1e4f-81534f1cbc00
Xtrain =  [process_image.(x_train_final) for x_train_final in  x_train_final]

# ╔═╡ ee384470-cf8f-11eb-3699-136df40034f9
Xtest =  [process_image.(x_test_final) for x_test_final in  x_test_final]

# ╔═╡ 3f919522-d20f-11eb-1cd8-ada7d103d832
Ytrain = vcat(["normal" for _ in 1:length(normal_labels_train)], ["pnuemonia" for _ in 1:length(pneuu_labels_train)])

# ╔═╡ 50560b52-cf7d-11eb-31cd-0b633c902542
h =[label for label in Ytrain]

# ╔═╡ 1e4b2850-d146-11eb-1dfd-af0a97d895a8
Ytest = vcat(["normal" for _ in 1:length(normal_labels_test)], ["pnuemonia" for _ in 1:length(pneuu_labels_test)])

# ╔═╡ 58ff4d80-cf72-11eb-15fb-776e29705082
size(Ytest)

# ╔═╡ 83cdaa80-cf71-11eb-12d4-674d410bf890
YStrain = [Flux.onehotbatch(Ytrains,["normal","pneumonia"]) for label in Ytrains]

# ╔═╡ 9b39e892-cf7c-11eb-1f39-07cdf37cb447
YStest = [Flux.onehotbatch(Ytests,["normal","pneumonia"]) for label in Ytests]

# ╔═╡ b58871f0-cf7a-11eb-2ad3-43f3b01e14d7
train_loader = DataLoader((Flux.batch(Xtrain),Flux.batch(YStrain)), shuffle=true,batchsize=120 )

# ╔═╡ c6810e20-cf72-11eb-0665-b3907a123bff
training_batch = (Flux.batch(Xtrain),Flux.batch(YStrain))


# ╔═╡ d9cb63b0-cf84-11eb-2b85-899d079c3729
testing_batch = (Flux.batch(Xtest),Flux.batch(YStest))

# ╔═╡ ac418f17-53a3-4b9d-a506-d48c0bdb7965
md"# Modelling"

# ╔═╡ 59ac6be0-cfbe-11eb-270d-d17cf34570a8
function LeNet5(; imgsize=(28,28,1), nclasses=2) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 16)
    
    return Chain(
          
				Conv((5, 5), imgsize[end]=>6, relu),
				MaxPool((2, 2)),
				Conv((5, 5), 6=>16, relu),
				MaxPool((2, 2)),
				x -> reshape(x, :, size(x, 4)),
				Dense(prod(out_conv_size), 120, relu), 
				Dense(120, 84, relu), 
				Dense(84, nclasses)
          )
end

# ╔═╡ bed9b610-d042-11eb-3896-01825d9101cc
training =Iterators.repeated(training_batch,10)

# ╔═╡ ca0c54c0-cf93-11eb-3968-3b54255ca62f
md"# Training "

# ╔═╡ 91253c10-cf72-11eb-1903-93dfb7dcfc91
begin
	 model = LeNet5()
	 loss(x,y) = Flux.logitcrossentropy(model(x),y)

	 ps = Flux.params(model)
     opt = ADAM(1e-4)
	 accuracy(x, y, f) = mean(Flux.onecold(f(x)) .== Flux.onecold(y))
	#@time Flux.train!(loss,ps,[training_batch],opt)
end

# ╔═╡ 98151c60-cf91-11eb-2c3e-217cdbaedd61
@epochs 20 Flux.train!(loss,ps,training,opt)

# ╔═╡ c7deff60-cf91-11eb-0e22-51d2c88462b1
md"# Evaluating model "

# ╔═╡ f548d250-cf91-11eb-37ca-d138e318523f
 
accs =accuracy(testing_batch..., model)

# ╔═╡ d7653f53-d4f0-447e-9773-5060cec960a6
function labels(predictions)
	final =[]
	  for  i in predictions
		 if  (i == "normal")
			#pneumonia  0
			push!(final,false)
		  else
			push!(final,true)
		end
	end 
	return final
end

# ╔═╡ 66086340-d86e-11eb-1854-7529e1e69274
function values(predictions)
	final =[]
	  for  i in predictions
		 if  i == 2 
			#pneumonia  1
			push!(final,1)
		  else
			push!(final,0)
		end
	end 
	return final
end

# ╔═╡ 31b775c0-d050-11eb-2009-d130a447f478
target = values(Flux.onecold(model(Flux.batch(Xtest))))

# ╔═╡ 06bad890-d86f-11eb-3993-893ea2515559
predict = labels(Ytest)

# ╔═╡ 5caeb5d1-ab80-4d0f-a288-bf40db938ac8
md"# Confusion Matrix "

# ╔═╡ 0ce6da2e-d86e-11eb-19d9-e978b623098c
Cm = ConfusionMatrix(target,predict)

# ╔═╡ 19404981-cc8e-48c1-ac9d-ca364d74038a
#how many values predicted where actually true positives
precisions = precision(target,predict)

# ╔═╡ 5dc58b20-cbfa-4a23-a3de-c63343744195
#
recalls = recall(target,predict)

# ╔═╡ Cell order:
# ╠═abe3c292-cef2-11eb-1d53-ad16b647f780
# ╠═af182110-cf8b-11eb-069a-3b6009262180
# ╠═fac58bf2-cef2-11eb-1e70-d38115051d1c
# ╠═a3a61330-d20d-11eb-1b4e-3102dcb98496
# ╠═2e113f0e-cf69-11eb-0c3f-7d36ff1444c6
# ╠═4c6cd2f0-cf6c-11eb-301d-97e7a2c385e5
# ╠═5f453200-cf6c-11eb-09a8-77afa3bc3e87
# ╠═5a494160-cf6c-11eb-1392-bfb16b22b670
# ╠═6ccb9c30-cef3-11eb-0fb7-1b3d978968e6
# ╠═57d815f0-d1bf-11eb-3b06-f90bec96e559
# ╠═b4a3b350-d153-11eb-3714-1790a254a71f
# ╠═ceda4440-cf56-11eb-1e4f-81534f1cbc00
# ╠═ee384470-cf8f-11eb-3699-136df40034f9
# ╠═58ff4d80-cf72-11eb-15fb-776e29705082
# ╠═50560b52-cf7d-11eb-31cd-0b633c902542
# ╠═3f919522-d20f-11eb-1cd8-ada7d103d832
# ╠═1e4b2850-d146-11eb-1dfd-af0a97d895a8
# ╠═83cdaa80-cf71-11eb-12d4-674d410bf890
# ╠═9b39e892-cf7c-11eb-1f39-07cdf37cb447
# ╠═b58871f0-cf7a-11eb-2ad3-43f3b01e14d7
# ╠═c6810e20-cf72-11eb-0665-b3907a123bff
# ╠═d9cb63b0-cf84-11eb-2b85-899d079c3729
# ╠═ac418f17-53a3-4b9d-a506-d48c0bdb7965
# ╠═59ac6be0-cfbe-11eb-270d-d17cf34570a8
# ╠═bed9b610-d042-11eb-3896-01825d9101cc
# ╠═ca0c54c0-cf93-11eb-3968-3b54255ca62f
# ╠═91253c10-cf72-11eb-1903-93dfb7dcfc91
# ╠═98151c60-cf91-11eb-2c3e-217cdbaedd61
# ╠═c7deff60-cf91-11eb-0e22-51d2c88462b1
# ╠═f548d250-cf91-11eb-37ca-d138e318523f
# ╠═d7653f53-d4f0-447e-9773-5060cec960a6
# ╠═66086340-d86e-11eb-1854-7529e1e69274
# ╠═31b775c0-d050-11eb-2009-d130a447f478
# ╠═06bad890-d86f-11eb-3993-893ea2515559
# ╠═5caeb5d1-ab80-4d0f-a288-bf40db938ac8
# ╠═0ce6da2e-d86e-11eb-19d9-e978b623098c
# ╠═19404981-cc8e-48c1-ac9d-ca364d74038a
# ╠═5dc58b20-cbfa-4a23-a3de-c63343744195
